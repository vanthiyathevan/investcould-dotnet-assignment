﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;

namespace InvestCouldExcercise
{
    [DataContract(Name = "repo")]
    class Result
    {
        [DataMember(Name = "Success")]
        public bool Success;
    }
    [DataContract(Name = "repo")]
    class DataSetResult
    {
        [DataMember(Name = "Value")]
        public double[] Value;
    }

    class Program
    {
        static IEnumerable<double> Multiply(double[][] arow, double[][] bcol)
        {
            for (int i = 0; i < arow.Length; i++)
            {
                for (int j = 0; j < bcol.Length; j++)
                {
                    double x = 0;
                    for (int k = 0; k < arow[i].Length; k++)
                    {
                        x += arow[i][k] * bcol[j][k];
                    }
                    yield return x;
                }
            }
        }
        static async Task<double[]> GetDataSet(string dataset, string type, int idx)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var dataStream = await client.GetStreamAsync($"https://recruitment-test.investcloud.com/api/numbers/{dataset}/{type}/{idx}");
                var serializer = new DataContractJsonSerializer(typeof(DataSetResult));
                var result = serializer.ReadObject(dataStream) as DataSetResult;
                return result.Value;
            }
        }
        static async Task<IEnumerable<double>> GetMatrix(int size)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var init = await client.GetStreamAsync($"https://recruitment-test.investcloud.com/api/numbers/init/{size}");
                var serializer = new DataContractJsonSerializer(typeof(Result));
                var result = serializer.ReadObject(init) as Result;
                if (!result.Success) throw new Exception("Failed");

                var range = Enumerable.Range(0, size);
                var arow = await Task.WhenAll(range.Select(i => GetDataSet("A", "row", i)));
                var bcol = await Task.WhenAll(range.Select(i => GetDataSet("B", "col", i)));
                return Multiply(arow, bcol);
            }
        }
        static string GetHash(string joined)
        {
            using (var md5Hash = MD5.Create())
            {
                var hash = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(joined));
                return BitConverter.ToString(hash).Replace("-", string.Empty);
            }
        }
        static async Task validate(string hash)
        {
            using (var client = new HttpClient())
            {
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var str = new StringContent(hash, Encoding.UTF8, "text/json");
                    var ans = await client.PostAsync("https://recruitment-test.investcloud.com/api/numbers/validate", str);
                    Console.WriteLine(await ans.Content.ReadAsStringAsync());
                }
            }
        }
        static void Main(string[] args)
        {
            try
            {
                var watch = new System.Diagnostics.Stopwatch();
                watch.Start();
                var t = GetMatrix(100).Result;
                var hash = GetHash(string.Join("", t));
                Console.WriteLine(hash);
                validate(hash).Wait();
                watch.Stop();
                Console.WriteLine($"Time: {watch.ElapsedMilliseconds} ms");
            }
            catch (System.Exception)
            {
                Console.WriteLine("Failed");
            }
        }
    }
}
